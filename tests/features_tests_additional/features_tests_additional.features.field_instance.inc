<?php
/**
 * @file
 * features_tests_additional.features.field.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function features_tests_additional_field_default_field_instances() {
  $field_instances = array();

  // Exported field: 'node-features_test-field_features_test_2'
  $field_instances['node-features_test-field_features_test_2'] = array(
    'bundle' => 'features_test',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_features_test_2',
    'label' => 'Test 2',
    'required' => 0,
    'settings' => array(
      'text_processing' => '0',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => '60',
      ),
      'type' => 'text_textfield',
      'weight' => '-3',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Test 2');

  return $field_instances;
}
