<?php
/**
 * @file
 * features_tests_additional.features.field.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function features_tests_additional_field_default_field_bases() {
  $field_bases = array();

  // Exported field: 'node-features_test-field_features_test_2'
  $field_bases['field_features_test_2'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_features_test_2',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'module' => 'text',
    'settings' => array(
      'max_length' => '255',
    ),
    'translatable' => '0',
    'type' => 'text',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Test 2');

  return $field_bases;
}
